#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "DoubleLinkList.h"

DoubleLinkList dList;
DoubleLinkList dl = dList.read_file();

// Test Search Algorithm
TEST_CASE("test the search result", "[Search]") {

	// check with the Head node
	std::string title = dl.get_head()->getData().get_title();
	Book book = dl.search_book(title);
	REQUIRE(book.get_title() == title);
	REQUIRE(book.get_title() != title + " ");

	// check with the second node
	title = dl.get_head()->getNext()->getData().get_title();
	book = dl.search_book(title);
	REQUIRE(book.get_title() == title);

	//check with the last node in the linkedlist
	Node* currentNode = dl.get_head();
	while (currentNode->getNext() != nullptr) {
		currentNode = currentNode->getNext();
	}
	title = currentNode->getData().get_title();
	book = dl.search_book(title);
	REQUIRE(book.get_title() == title);
	REQUIRE(currentNode->getNext() == nullptr);
}

// Test Adding Algorithm
TEST_CASE("Test Book Class after Adding to the linkedList", "[Adding]") {
	Book book1 = dl.get_head()->getData();
	std::string title = dl.get_head()->getData().get_title();
	Book book2 = dl.search_book(title);
	REQUIRE(book1.get_title() == book2.get_title());
	REQUIRE(book1.get_authors() == book2.get_authors());
	REQUIRE(book1.get_ISBN() == book2.get_ISBN());
	REQUIRE(book1.get_qty() == book2.get_qty());

	Book book3;
	book3.set_title("Learning ipython");
	book3.set_authors("jane bond");
	book3.set_ISBN("888777000999");
	book3.set_qty(6);
	dl.insertBack(book3);

	Node* currentNode = dl.get_head();
	while (currentNode->getNext() != nullptr) {
		currentNode = currentNode->getNext();
	}
	std::string isbn = currentNode->getData().get_ISBN();
	REQUIRE(book3.get_ISBN() == isbn);
}


TEST_CASE("Test Loading Data into DoubleLinkList", "[DoubleLinkList()]") {
	dl = dl.read_file();
	REQUIRE(dl.get_head()->getData().get_qty() > 0);
	REQUIRE(dl.get_head()->getData().get_ISBN().length() >= 1);
	REQUIRE(dl.get_head()->getData().get_title() == dl.get_head()->getData().get_title());
	REQUIRE(dl.get_head()->getData().get_authors().length() > 0);

}
// Test Delete Algorithm
TEST_CASE("Test Delete Algorithm from DoubleLinkList", "[Delete]") {
	dl = dl.read_file();
	// Remove first node
	
	Book book = dl.search_book(dl.get_head()->getData().get_title());
	std::string title = book.get_title();
	dl.removeItem(book);
	REQUIRE(dl.get_head()->getData().get_title() != title);

}
