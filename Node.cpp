#include "Node.h"

Node::Node(Book data, Node* next, Node* previous) {
    this->data = data;
    this->next = next;
    this->previous = previous;
}

Book Node::getData() { return this->data; }
Node* Node::getNext() { return this->next; }
Node* Node::getPrevious() { return this->previous; }
void Node::setNext(Node* next) { this->next = next; }
void Node::setPrevious(Node* previous) { this->previous = previous; }

