#ifndef DOUBLELINKEDLIST_H
#define DOUBLELINKEDLIST_H
#include <vector>
#include "Node.h"

class DoubleLinkList {

private:
    Node* head = nullptr;
public:
    void insertFront(Book item);
    void insertBack(Book item);
    Book removeItem(Book item);
    Book removeFront();
    Book removeBack();
    bool empty();
    Book search_book(std::string title);
    Node* get_head();
    void search_for_book(std::string title);
    DoubleLinkList read_file();
    DoubleLinkList add_book(DoubleLinkList dl);
    DoubleLinkList delete_book(std::string title);

};
#endif 
