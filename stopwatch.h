#ifndef _STOPWATCH_H
#define _STOPWATCH_H

#include<ctime>
class stopwatch {

private:
	clock_t starttime;
public:
	void start();
	double stop();

};

#endif
