//#include <iostream>
#include "DoubleLinkList.h"
//#include "Node.h"

Node* DoubleLinkList::get_head() {
    return this->head;
}

void DoubleLinkList::insertFront(Book item) {
    Node* newnode = new Node(item, this->head, nullptr);
    if (!this->empty())
        this->head->setPrevious(newnode);
    this->head = newnode;
}

Book DoubleLinkList::search_book(std::string title) {
    Book b;
    Node* currentNode;
    if (this->empty())
        return b;

    currentNode = this->head;
    if (currentNode->getData().get_title() == title)
        return currentNode->getData();

    while (currentNode->getNext() != nullptr) {
        currentNode = currentNode->getNext();
        if (currentNode->getData().get_title() == title)
            return currentNode->getData();

    }

    return b;
}

void DoubleLinkList::insertBack(Book item) {
    Node* currentNode;
    if (this->empty())
    {
        insertFront(item);
        return;
    }
    currentNode = this->head;
    while (currentNode->getNext() != nullptr)
        currentNode = currentNode->getNext();
    Node* newnode = new Node(item, nullptr, currentNode);
    currentNode->setNext(newnode);
}


Book DoubleLinkList::removeItem(Book item) {
    //Book item = Book();
    Node* currentNode = this->head;
    Node* previousNode = nullptr;
    if (this->empty()) {
        std::cout << "Error: List empty\n";
        return item;
    }

    while (currentNode->getNext() != nullptr && currentNode->getData().get_title() != item.get_title()) {
        previousNode = currentNode;
        currentNode = currentNode->getNext();
    }
    // if the needed item is the head, then remove it
    if (previousNode == nullptr) {
        previousNode = this->head;
        this->head = nullptr;
        this->head = previousNode->getNext();

    }
    else { // deleting
        // to check if not the last node in the linkedlist
        if (currentNode->getNext() != nullptr) {
            previousNode->setNext(currentNode->getNext());
            currentNode->getNext()->setPrevious(previousNode);
        }
        else {
            //to delete the last node in the linkedlist
            previousNode->setNext(nullptr);
        }
    }
    item = currentNode->getData();
    delete currentNode;
    return item;
}


Book DoubleLinkList::removeFront() {
    Book item = Book();
    Node* currentNode;
    if (this->empty()) {
        std::cout << "Error: List empty\n";
        return item;
    }

    currentNode = this->head;
    item = this->head->getData();
    this->head = this->head->getNext();
    if (this->head != nullptr)
        this->head->setPrevious(nullptr);
    delete currentNode;
    return item;
}

Book DoubleLinkList::removeBack() {
    Book item = Book();
    Node* currentNode = this->head;
    Node* previousNode = nullptr;
    if (this->empty()) {
        std::cout << "Error: List empty\n";
        return item;
    }

    while (currentNode->getNext() != nullptr) {
        previousNode = currentNode;
        currentNode = currentNode->getNext();
    }
    if (previousNode == nullptr) {
        this->head = nullptr;
    }
    else {
        previousNode->setNext(nullptr);
    }
    item = currentNode->getData();
    delete currentNode;
    return item;
}

bool DoubleLinkList::empty() {
    return this->head == nullptr;
}


DoubleLinkList DoubleLinkList::read_file() {
    DoubleLinkList dl;
    Book* book = new Book("", "", "", 0);
    std::vector<std::string> vec;
    std::ifstream inputFile("books.txt");
    std::string productArray[4];
    std::string line;
    while (getline(inputFile, line)) {

        size_t found = line.find("\t");
        if (found != std::string::npos) {
            std::string str2 = line.substr(0, found);
            productArray[0] = str2;
            //std::cout << str2 << "\n";
        }
        size_t found2 = found + 1;
        found = line.find("\t", found2);
        if (found != std::string::npos) {
            std::string str2 = line.substr(found2, found - found2);
            productArray[1] = str2;
        }

        found2 = found + 1;
        found = line.find("\t", found2);
        if (found != std::string::npos) {
            std::string str2 = line.substr(found2, found - found2);
            productArray[2] = str2;
        }

        found2 = found + 1;
        found = line.find("\t", found2);
        if (found != std::string::npos) {
            std::string str2 = line.substr(found2, found - found2);
            productArray[3] = str2;
        }
        /*
        std::istringstream iss(line);
        for (int i = 0; i < (sizeof(productArray) / sizeof(productArray[0])); i++)
        {
            std::string subs;
            iss >> subs;
            productArray[i] = subs;
        }
        */
        book->set_title(productArray[0]);
        book->set_authors(productArray[1]);
        book->set_ISBN(productArray[2]);
        book->set_qty(stoi(productArray[3]));
        //Node* newnode = new Node(book, dl.get_head(), nullptr);
        //std::cout <<productArray[0] << "\t" << productArray[1] << "\t" << productArray[2] << "\t" << productArray[3] << "\n";
        dl.insertBack(*book);
    }
    return dl;
}

void DoubleLinkList::search_for_book(std::string title) {
    Book book;
    DoubleLinkList dl = read_file();
    book = dl.search_book(title);
    if (book.get_title().length() > 1) {
        std::cout << "\n  Title  : " << book.get_title() << std::endl;
        std::cout << "  Authors: " << book.get_authors() << std::endl;
        std::cout << "  ISBN   : " << book.get_ISBN() << std::endl;
        std::cout << "  Qty    : " << book.get_qty() << std::endl;
    }
    else {
        std::cout << "The Book: [" << title << "] not found!" << std::endl;
    }
}


DoubleLinkList DoubleLinkList::add_book(DoubleLinkList dList) {
    int qty;
    //std::cin.ignore();
    std::string title, authors, ISBN;
    std::cout << "Enter title: ";
    getline(std::cin, title);
    std::cout << "Enter authors: ";
    getline(std::cin, authors);
    std::cout << "Enter ISBN: ";
    getline(std::cin, ISBN);
    std::cout << "Enter qty: ";
    std::cin >> qty;
    Book book = Book(title, authors, ISBN, qty);
    dList.insertBack(book);
    std::ofstream outputFile;
    //std::ofstream outputFile;
    outputFile.open("books.txt", std::ios::out | std::ios::app);

    outputFile << title << "\t" << authors << "\t" << ISBN << "\t" << qty << "\t" << "\n";

    outputFile.close();
    std::cout << "\nBook '" << title << "' saved successfully!\n\n";
    return dList;
}



DoubleLinkList DoubleLinkList::delete_book(std::string title) {
    Book book, b;
    DoubleLinkList dl = read_file();
    book = dl.search_book(title);
    // if the item is found, then the length of its title will be more than one letter

    std::cout << "\n";
    if (book.get_title().length() > 1) {
        b = dl.removeItem(book);
        std::ofstream outputFile;
        outputFile.open("Books.txt");

        Node* currentNode = dl.get_head();
        while (currentNode != nullptr) {
            std::string title = currentNode->getData().get_title();
            std::string authors = currentNode->getData().get_authors();
            std::string isbn = currentNode->getData().get_ISBN();
            int qty = currentNode->getData().get_qty();
            outputFile << title << "\t" << authors << "\t" << isbn << "\t" << qty << "\t\n";
            currentNode = currentNode->getNext();
        }
        outputFile.close();

        std::cout << "The Book:[" << b.get_title() << "] Removed Successfully!\n";
    }
    else
        std::cout << "The Book:[" << title << "] Not Found!\n";
    return dl;
}
