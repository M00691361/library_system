#ifndef NODE_H
#define NODE_H
#include "Book.h"
#include <iostream>
#include <iostream>

class Node {

private:
    Book data;
    Node* next;
    Node* previous;
public:
    Node(Book data, Node* next, Node* previous);
    Book getData();
    Node* getNext();
    Node* getPrevious();
    void setNext(Node* next);
    void setPrevious(Node* previous);

};
#endif 
