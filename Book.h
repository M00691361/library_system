#ifndef BOOK_H
#define BOOK_H
#include <sstream>
#include <iostream>
#include <fstream>

class Book
{
private:
    std::string title;
    std::string authors;
    std::string ISBN;
    int qty;
public:

    Book(std::string title, std::string authors, std::string ISBN, int qty);

    //void write_to_file(int id);

    Book();
    ~Book();

    // Getter
    std::string get_title();
    std::string get_authors();
    std::string get_ISBN();
    int get_qty();

    // Setter
    void set_title(std::string title);
    void set_authors(std::string authors);
    void set_ISBN(std::string ISBN);
    void set_qty(int qty);

  // void add_book();

};
#endif 
