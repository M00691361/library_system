#include "Book.h"


//#include <iomanip>

Book::Book(std::string title, std::string authors, std::string ISBN, int qty)
{
    this->title = title;
    this->authors = authors;
    this->ISBN = ISBN;
    this->qty = qty;
}

Book::Book()
{
    title = "";
    authors = "";
    ISBN = "";
    qty = 0;
}

Book::~Book()
{

}

// Getter
std::string Book::get_title() { return this->title; }

std::string Book::get_authors() { return this->authors; }
std::string Book::get_ISBN() { return this->ISBN; }
int Book::get_qty() { return this->qty; }

// Setter
void Book::set_title(std::string title) { this->title = title; }
void Book::set_authors(std::string authors) { this->authors = authors; }
void Book::set_ISBN(std::string ISBN) { this->ISBN = ISBN; }
void Book::set_qty(int qty) { this->qty = qty; }
