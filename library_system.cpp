#include <iostream>
#include <stdlib.h>
#include <sstream>
#include "DoubleLinkList.h"
#include <vector>
#include "stopwatch.h"



void calculate_algorithms_time(DoubleLinkList dlist, int option);
int main() {
    DoubleLinkList dlist;
    // dl linked list will hold all books from the file
    DoubleLinkList dl = dlist.read_file();
    int choise = 0;
    while (choise != 5) {
        std::cout << "\n\n\n++++++++++     M E N U     ++++++++++\n";

        std::cout << "1. Search Book By title\n";
        std::cout << "2. Add a new Book\n";
        std::cout << "3. Remove a Book\n";
        std::cout << "4. Calculate Time consumed\n";
        std::cout << "5. Exit\n";

        std::cout << "\nSelect an option : ";

        std::cin >> choise;
        std::cin.ignore();
        std::string title, authors, ISBN;
        // Search
        if (choise == 1) {
            std::cout << "Enter title: ";
            getline(std::cin, title);

            dlist.search_for_book(title);
        }
        // Adding
        else if (choise == 2) {
            dl=dl.add_book(dl);
        }
        // Remove
        else if (choise == 3) {
            std::cout << "Enter title: ";
            getline(std::cin, title);
           dl = dl.delete_book(title);
        }

        else if (choise == 4) { // calculate time
            int opt;
            std::string option;
            //std::cin.ignore();
            std::cout << "1. calculate time ( Search Algorithm) \n";
            std::cout << "2. calculate time ( Delete Algorithm) \n";
            std::cout << "\t Choose one task: ";
            
            getline(std::cin, option);
            opt = stoi(option);
            
            calculate_algorithms_time( dl, opt);          
        }

        else if (choise == 5)
            exit(0);
    }
}

void calculate_algorithms_time(DoubleLinkList dlist, int option) {
    stopwatch* watch = new stopwatch();

    int number_of_books = 0;
    //int count = 1;
    Node* currentNode = dlist.get_head();
    while (currentNode->getNext() != nullptr) {
        number_of_books += 1;
        currentNode = currentNode->getNext();
    }
    double timeConsumed;
    std::string  algorithm_name = "";
    
    Book book;
    std::string title="";
    // get last book from the file
    //currentNode = dlist.get_head();
    std::cout << "\tbook-position" << "\t" << "time-Consumed" << std::endl;
    std::cout << "\t-------------" << "\t" << "-------------" << std::endl;
    for (int i = 1; i <= number_of_books; i=i*2) {
        dlist = dlist.read_file();
        currentNode = dlist.get_head();
        int c = 1;
        if ( number_of_books <= i*2)
            i = number_of_books;
        while (currentNode != nullptr) {
            if (c == i) {
                title = currentNode->getData().get_title();
                if (option == 1) {
                    watch->start();
                    book = dlist.search_book(title);
                    timeConsumed = watch->stop();
                    //algorithm_name = "Searching";
                }
                else {
                    book = dlist.search_book(title);
                    watch->start();
                    dlist.removeItem(book);
                    timeConsumed = watch->stop();
                    //algorithm_name = "Removing";
                    dlist = dlist.read_file();
                }
                break;
            }
            c += 1;
            currentNode = currentNode->getNext();
        }

        std::cout << "\t\t" <<c<< "\t\t" << timeConsumed << std::endl;

    }

}

