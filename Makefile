CXX = g++
CXXFLAGS = -g -Wall -Wextra

.PHONY : all
all : library_system

library_system : library_system.cpp DoubleLinkList.o Node.o Book.o stopwatch.o
	$(CXX) $(CXXFLAGS) -o $@ $^

DoubleLinkList.o : DoubleLinkList.cpp DoubleLinkList.h Node.o Book.o
	$(CXX) $(CXXFLAGS) -c $^

Node.o : Node.cpp Node.h Book.o
	$(CXX) $(CXXFLAGS) -c $^

Book.o : Book.cpp Book.h
	$(CXX) $(CXXFLAGS) -c $<

stopwatch.o : stopwatch.cpp stopwatch.h
	$(CXX) $(CXXFLAGS) -c $<

.PHONY : clean
clean :
	rm *.o library_system
